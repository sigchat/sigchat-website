# SiGChat

Simple GPG Chat

## Project Scope

The main scope of this project is to provide an user friendly chat that uses only GPG to send and retrieve messages. The app must have the following capabilities:

- authenticate users with a GPG challenge
- allow key to read retreive messages sent and received
- allow key to send messages (with a limit to avoid spam)
- store messages sent from one key to another
- open a gRPC chat window so that messages can be send and received in real time

## Contributions

Before starting work, an issue has to be opened within the appropriate project.

Most of the logic will be written in GoLang, so be sure that you respect the following rules:

- code is completely `gofmt`'d
- test units are written and `go test` passes
- the code has comments and is easy to read (the variable names are nice)
